defmodule LiveViewStudioWeb.SandboxLive do
  use LiveViewStudioWeb, :live_view

  import Number.Currency

  def mount(_params, _session, socket) do
    socket =
      assign(socket,
        length: "0",
        width: "0",
        depth: "0",
        weight: 0.0,
        price: nil
      )

    {:ok, socket}
  end

  def handle_event("get-quote", _params, socket) do
    price = LiveViewStudio.Sandbox.calculate_price(socket.assigns.weight)

    socket =
      assign(socket,
        price: price
      )

    {:noreply, socket}
  end

  def handle_event("calculate", %{"length" => length, "width" => width, "depth" => depth}, socket) do
    weight = LiveViewStudio.Sandbox.calculate_weight(length, width, depth)

    socket =
      assign(socket,
        length: length,
        width: width,
        depth: depth,
        weight: weight,
        price: nil
      )

    {:noreply, socket}
  end
end
