defmodule LiveViewStudioWeb.LightLive do
  use LiveViewStudioWeb, :live_view

  import LiveViewStudio.LightLive

  def mount(_params, _session, socket) do
    socket = assign(socket, brightness: 10, temp: "3000")
    {:ok, socket}
  end

  def handle_event("off", %{"value" => value}, socket) do
    {:noreply, assign(socket, brightness: parse_value(value))}
  end

  def handle_event("on", %{"value" => value}, socket) do
    {:noreply, assign(socket, brightness: parse_value(value, 100))}
  end

  def handle_event("up", %{"value" => value}, socket) do
    value = parse_value(value, 5)
    brightness = calculate_brightness(socket.assigns.brightness, value)

    {:noreply, assign(socket, brightness: brightness)}
  end

  def handle_event("down", %{"value" => value}, socket) do
    value = parse_value(value, 5)
    brightness = calculate_brightness(socket.assigns.brightness, value)

    {:noreply, assign(socket, brightness: brightness)}
  end

  def handle_event("fire", _params, socket) do
    {:noreply, assign(socket, brightness: Enum.random(0..100))}
  end

  def handle_event("slide", params, socket) do
    {:noreply, assign(socket, brightness: parse_value(params["brightness"], 0))}
  end

  def handle_event("temp", %{"temp" => temp}, socket) do
    {:noreply, assign(socket, temp: temp)}
  end

  defp temp_color("3000"), do: "#F1C40D"
  defp temp_color("4000"), do: "#FEFF66"
  defp temp_color("5000"), do: "#99CCFF"
  defp temp_color(_), do: "#FDEO4F"
end
