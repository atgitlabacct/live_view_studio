defmodule LiveViewStudio.LightLive do
  def calculate_brightness(current, change) do
    max(min(current + change, 100), 0)
  end

  def parse_value(value, default \\ 0) do
    case Integer.parse(value) do
      {int_value, _} -> int_value
      :error -> default
    end
  end
end
